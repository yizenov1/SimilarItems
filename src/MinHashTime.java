import java.util.concurrent.TimeUnit;

//TODO: do I need to create an instance of this class?
public class MinHashTime {

	//TODO: this is static
    public static void timer(String folder, int numPermutations) {
        
    	//TODO: time fixing is wrong
    	MinHash minHash = new MinHash(folder, numPermutations);
        String[] fileNames = minHash.allDocs();
        
        long startTime, estimatedTime;
        
        //TODO: time is nanoseconds
        startTime = System.nanoTime();
        for (String doc1: fileNames) {
            for (String doc2: fileNames) {
                if (doc1.equals(doc2)) continue;
                minHash.exactJaccard(doc1, doc2);
            }
        }
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(String.format("Time spent to calculate exactJaccard (seconds): %x", 
                TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.NANOSECONDS)));
        
        startTime = System.nanoTime();
        minHash.BuildMinHash();
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(String.format("Time spent to build MinHash matrix (seconds): %x", 
                TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.NANOSECONDS)));
        
        startTime = System.nanoTime();
        for (String doc1: fileNames) {
            for (String doc2: fileNames) {
                if (doc1.equals(doc2)) continue;
                minHash.approximateJaccard(doc1, doc2);
            }
        }
        estimatedTime = System.nanoTime() - startTime;
        System.out.println(String.format("Time spent to calculate approxJaccard (seconds): %x", 
                TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.NANOSECONDS)));
    }
}
