import java.io.*;
import java.math.BigInteger;
import java.util.*;

//TODO: Do HashTable,Map,Set use hashing when they store data?
//Enumerator in for-each loop
public class MinHash {
	   
    private Map<String, Integer> terms;
    private Map<String, Integer> fileNames;
    private Map<String, Set<String>> docTerms;
    
    private int numPermutations;   
    private int prime_number;
    private int[] hash_a, hash_b;
    private Random generator;
    
    private int[][] minHashMatrix;
	
	MinHash(String folder, int numPermutations) {
	    terms = new HashMap<String, Integer>();
	    this.numPermutations = numPermutations;
	    
	    fileNames = new HashMap<String, Integer>();
	    
	    docTerms = new HashMap<String, Set<String>>();
	    ReadDocuments(folder);
	}
	
	//TODO: does it have to be array or other collections are fine too?
	String[] allDocs() {
		return fileNames.keySet().toArray(new String[0]);
	}
	
	double exactJaccard(String file1, String file2) {
	    
	    double l2file1 = 0.0, l2file2 = 0.0, dotProduct = 0.0;
        
        Set<String> file1Terms = docTerms.get(file1);
        Set<String> file2Terms = docTerms.get(file2);
        
        for (String file1Term: file1Terms) {
        	if (file2Terms.contains(file1Term)) dotProduct++;
        }
	    
        l2file1 = Math.sqrt(file1Terms.size());
        l2file2 = Math.sqrt(file2Terms.size());
	    
	    return dotProduct / (l2file1 * l2file1 + l2file2 * l2file2 - dotProduct);
	}
	
	int[] minHashSig(String fileName) {
	    int file_index = fileNames.get(fileName);
	    int[] signature = new int[numPermutations];
	    
	    for(int i = 0; i < numPermutations; i++) {
	        signature[i] = minHashMatrix[file_index][i];
	    }
	    return signature;
	}
	
	//TODO: this can be faster
	double approximateJaccard(String file1, String file2) {
	    int[] file1Signature = minHashSig(file1);
	    int[] file2Signature = minHashSig(file2);
	    
	    int counter = 0, match_counter = 0;
	    while (counter < numPermutations) {
	        if (file1Signature[counter] == file2Signature[counter]) match_counter++;
	        counter++;
	    }
	    return match_counter / (double) numPermutations;
	}
	
	//TODO: why?
	int[][] minHashMatrix() {
	    
		//return minHashMatrix;
        String[] allDocs = this.allDocs();
        int[][] minHashMatrix = new int[allDocs.length][numPermutations];
        
        for (int i = 0; i < allDocs.length; i++) {
            minHashMatrix[i] = minHashSig(allDocs[i]);
        }
        return minHashMatrix;
	}
	
	int numTerms() {
	    return terms.size();
	}
	
	int numPermutations() {
	    return numPermutations;
	}
	
	private void ReadDocuments(String folderPath) {
	    File folder = new File(folderPath);
	    FileReader fileReader = null;
	    BufferedReader bufferedReader = null;
	    
	    List<String> words; //all words of a particular document with potential of duplicates
	    Set<String> allWords; //stores all distinct words of a particular document
	    
	    int file_index = 0, term_counter = 0;
	    String line;
	    
	    try {
	        //TODO: why do I need final?
	        for (final File fileEntry : folder.listFiles()) {
	            fileReader = new FileReader(fileEntry);
	            bufferedReader = new BufferedReader(fileReader);
	            
	            allWords = new HashSet<String>();
	            
	            //indexing the files into HashMap<String, Integer>
	            fileNames.put(fileEntry.getName(), file_index);
	            file_index++;
	            
	            while((line = bufferedReader.readLine()) != null) {
	                words = PreProcessing(line); //pre-processing the line
	                for(String term: words) {
	                	
	                	if (!allWords.contains(term)) allWords.add(term);
	                	
	                    if (!terms.containsKey(term)) {
	                    	terms.put(term, term_counter);
	                    	term_counter++;
	                    }
	                }
	            }
	            
	            //putting all distinct words of a particular document
	            docTerms.put(fileEntry.getName(), allWords);
	            
	            //TODO: do I have to close them every time?
	            bufferedReader.close();
	            fileReader.close();
	        }
    	            
	    } catch(FileNotFoundException ex) {
	        System.out.println("Unable to open file '" + folderPath + "'");
	    } catch (IOException e) {
	        System.out.println("Error reading file '" + folderPath + "'");
	    } finally {        
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
	    }
	}
	
	private List<String> PreProcessing(String line) {
	    String[] words = line.toLowerCase().split(" ");
	    
	    List<String> preProcessedWords = new LinkedList<String>();
	    
	    int length;
	    char firstChar, lastChar;
	    boolean isFirstCharPunctuation;
	    
	    for(String word: words) {
	        length = word.length();
	        if (length < 3) continue;
	        isFirstCharPunctuation = false;
	        
	        firstChar = word.charAt(0);
	        if (firstChar == '.' || firstChar == ',' || firstChar == ':' 
	                || firstChar == ';' || firstChar == '\'') isFirstCharPunctuation = true;
	            
	        lastChar = word.charAt(length - 1);
	        if (lastChar == '.' || lastChar == ',' || lastChar == ':' 
	                || lastChar == ';' || lastChar == '\'') {
	            if (isFirstCharPunctuation) { preProcessedWords.add(word.substring(1, length - 2)); }
	            else { preProcessedWords.add(word.substring(0, length - 2)); }
	        } else {
	            if (isFirstCharPunctuation) { preProcessedWords.add(word.substring(1, length - 1)); }
                    else { preProcessedWords.add(word.substring(0, length - 1)); }
	        }
	    }
	    return preProcessedWords;
	}

	
	private int FindPrimeNumber(int setSize) {
            
        int primeNumber = setSize;
        int counter = 0;
        
        //TODO: do I need bitsPerElement?
        while (primeNumber < setSize) {
            for(int num = primeNumber; num >= 1; num--) {
                if(primeNumber % num == 0) {
                    counter = counter + 1;
                    if (counter > 2) break;
                }
            }
        
            if (counter == 2) return primeNumber; 
            primeNumber++;
        }
        
        return primeNumber;
    }
	
	//constructing MinHash matrix
	public void BuildMinHash() {
	    
	    hash_a = new int[numPermutations];
        hash_b = new int[numPermutations];
        
        int range = terms.size();
        prime_number = FindPrimeNumber(range);
        
        generator = new Random();
        for (int i = 0; i < numPermutations; i++) {
            hash_a[i] = generator.nextInt(range);
            hash_b[i] = generator.nextInt(range);
        }
        
        minHashMatrix = new int[fileNames.size()][numPermutations];
            
	    //TODO: we need indices of each term
	    int min_value, hash, fileIndex;
    	for (String doc: fileNames.keySet()) {
    		for (int i = 0; i < numPermutations; i++) {
            
    			min_value = Integer.MAX_VALUE;
    			fileIndex = fileNames.get(doc);
	            
    			//permutation process
	        	for (String term: docTerms.get(doc)) {
	                //TODO: can this be integer?
	                hash = (int) (hash_a[i] * terms.get(term) + hash_b[i]) % prime_number;
	                BigInteger big = BigInteger.valueOf(hash);
	                hash = big.intValue();
	                if (hash < min_value) min_value = hash;
	            }
	        	
	        	//finding minimum within a permutation
	            if (min_value == Integer.MAX_VALUE) {
	                minHashMatrix[fileIndex][i] = 0;
	            } else {
	                minHashMatrix[fileIndex][i] = min_value;
	            }
    		}
	    } 
	}
}
