import java.io.File;
import java.util.*;

public class LaunchHashing {

	//TODO: are we allowed to create an extra class for main function?
	//TODO: what is "Cannot make a static reference to the non-static method"?
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String folder_space = "src/space/space";
		String folder_articles = "src/articles/articles";
		String folder_f16pa2 = "src/F16PA2/F16PA2";
		
		int[] numPermutations = {400, 600, 800};
		double[] errors = {0.04, 0.07, 0.09};
		
		for (int i = 0; i < numPermutations.length; i++) {
			for (int j = 0; j < errors.length; j++) {
				System.out.println(String.format("NumPermutations: %s, error rate: %.2f, numDiffPairs: %d",
					numPermutations[i], errors[j], MinHashAccuracy.accuracy(folder_space, numPermutations[i], errors[j])));
			}
		}
		
		//TODO: do I need to clean memory first?
		MinHashTime.timer(folder_space, numPermutations[1]);
		
		//TODO: can I pick first 10 documents?
		//TODO: pick distinct ones
		List<String> sampleDocs = ReadDocuments(folder_articles, 10);
		List<String> nearDuplicates;
		
		//TODO: what is numPermutations, bands, threshold?
		double simThreshold = 0.9;
		
		int index = 1;
		//TODO: do I need to clean memory first?
		for (String doc: sampleDocs) {
			nearDuplicates = NearDuplicates.nearDuplicatesDetector(folder_f16pa2, numPermutations[1], simThreshold, doc);
			System.out.println(String.format(index + ". Near duplicates of %s:", doc));
			index++;
			for (int i = 0; i < nearDuplicates.size(); i++) {
				System.out.println(String.valueOf(i + 1) + ". " + nearDuplicates.get(i));
			}
			System.out.println();
		}
	}
	
	private static List<String> ReadDocuments(String folderPath, int numFiles) {
	    
		//TODO: do i close File?
		File folder = new File(folderPath);
		
	    List<String> sampleDocs = new LinkedList<String>();
	    Random generator = new Random();
	    File[] files = folder.listFiles();
	    int size = files.length;
	    int rand;
	    
	    for(int i = 0; i < numFiles; i++) {
		    rand = generator.nextInt(size);
		    sampleDocs.add(files[rand].getName());
	    }

	    return sampleDocs;
	}

}
