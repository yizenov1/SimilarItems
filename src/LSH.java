import java.util.*;

public class LSH {
	
	private int[][] minHashMatrix;
	private String[] docNames;
	private int bands;
	
	List<Map<String, Set<String>>> hashTables;

	public LSH(int[][] minHashMatrix, String[] docNames, int bands) {
		this.minHashMatrix = minHashMatrix;
		this.docNames = docNames;
		this.bands = bands;
		
		hashTables = new LinkedList<Map<String, Set<String>>>();
		BuildLSH();
	}
	
	//TODO: I don't know if this is correct
	public String[] nearDuplicatesOf(String docName) {
		List<String> nearDuplicateDocs = new LinkedList<String>();
		Set<String> aux_set = new HashSet<String>();
		
		for (Map<String, Set<String>> map: hashTables) {
			
			for (Set<String> term_docs: map.values()) {
				if (!term_docs.contains(docName)) continue;
				
				for (String doc: term_docs) {
					if (doc.equals(docName)) continue;
					
					if (!aux_set.contains(doc)) {
						nearDuplicateDocs.add(doc);
						aux_set.add(doc);
					}
				}
			}
		}
		return nearDuplicateDocs.toArray(new String[0]);
	}
	
	private void BuildLSH() {
		
		//TODO: number of permutations/bands has to be bigger/smaller than another
		
		int k = minHashMatrix[0].length / bands;
		int r = k;
		
		//TODO: how about the rest of the hash functions?
		Map<String, Set<String>> hashTable;
		
		//TODO: not a good idea
		Set<String> set;
		
		for (int i = 0; i < bands; i++) {
			hashTable = new HashMap<String, Set<String>>();
			for (int j = 0; j < docNames.length; j++) {
				
				String hashCode = "";
				for (int t = r - k; t < r; t++) {
					hashCode += minHashMatrix[j][t];
				}
				
				//TODO: is this okay to use hashCode?
				//TODO: so I don't need any prime_number to modulo?
				
 				if (!hashTable.containsKey(hashCode)) {
					set = new HashSet<String>();
					set.add(docNames[j]);
					hashTable.put(hashCode, set);
				} else {
					set = hashTable.get(hashCode);
					set.add(docNames[j]);
				}
			}
			hashTables.add(hashTable);
			r += k;
		}
	}
}
