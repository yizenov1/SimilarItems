import java.util.*;

public class NearDuplicates {

	public static List<String> nearDuplicatesDetector(String folder, int numPermutations, 
			double simThreshold, String docName) {
		
		List<String> trueNearDuplicates = new LinkedList<String>();
		
		//TODO: why to recreate again?
		MinHash minHash = new MinHash(folder, numPermutations);
		minHash.BuildMinHash();
		
		int bands = getNumOfBands(simThreshold, numPermutations);
		LSH lsh = new LSH(minHash.minHashMatrix(), minHash.allDocs(), bands); //21
		
		String[] nearDuplicates = lsh.nearDuplicatesOf(docName);
		
		int index = 0;
		for (String nearDupl: nearDuplicates) {
			System.out.println(index++ + ". " + nearDupl);
		}
		System.out.println();
		
		for (String nearDuplicate: nearDuplicates) {
			if (minHash.approximateJaccard(docName, nearDuplicate) > simThreshold) {
				trueNearDuplicates.add(nearDuplicate);
			}
		}
		
		return trueNearDuplicates;
	}
	
	private static int getNumOfBands(double threshold, int numPermutations) {
		double result = 0.0;
		int bands = numPermutations;
		
//		while (Math.abs(result - threshold) > 0.1) {
//			result =  1 / Math.pow(bands, (double) bands / numPermutations); 
//			bands -= 1;
//		}
		
		double minDifference = Double.MAX_VALUE;
		for(int i=1; i<=numPermutations; i++) {
			result =  1 / Math.pow(i, (double) i / numPermutations);
			if(Math.abs(result-threshold) < minDifference) {
				bands = i;
				minDifference = result - threshold;
			}
		}
		
		return bands;
	}
}
