//TODO: do I need to create an instance of this class?
public class MinHashAccuracy {

	//TODO: this is static
    public static int accuracy(String folder, int numPermutations, double error) {
		if (error < 0 || error > 1) return 0;
		
		MinHash minHash = new MinHash(folder, numPermutations);
		minHash.BuildMinHash();
		
		String[] fileNames = minHash.allDocs();
		
		double exactJaccard, approxJaccard;
		int badEstimate_counter = 0;
		
		for (String doc1: fileNames) {
		    for (String doc2: fileNames) {
		        if (doc1.equals(doc2)) continue;
		        exactJaccard = minHash.exactJaccard(doc1, doc2);
		        approxJaccard = minHash.approximateJaccard(doc1, doc2);
		        if (Math.abs(exactJaccard - approxJaccard) > error) badEstimate_counter++;
		    }
		}
		
		return badEstimate_counter;
    }
}
